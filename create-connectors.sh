#!/bin/bash

# Create JDBC sink connector

curl -X "POST" "http://localhost:18083/connectors/" \
     -H "Content-Type: application/json" \
     -d '{
             "name": "postgres_writing_sink",
             "config": {
                 "connector.class": "io.confluent.connect.jdbc.JdbcSinkConnector",
                 "connection.url": "jdbc:postgresql://postgres-writing-aggregate:5432/postgres?user=postgres&password=postgres",
                 "auto.create":"false",
                 "auto.evolve":"true",
                 "insert.mode":"upsert",
                 "delete.enabled": "true",
                 "pk.mode": "record_key",
                 "pk.fields": "id",
                 "table.name.format": "author",
                 "topics": "postgres.public.profile",
                 "transforms": "ReplaceField",
                 "transforms.ReplaceField.type": "org.apache.kafka.connect.transforms.ReplaceField$Value",
                 "transforms.ReplaceField.whitelist": "username,name,__deleted"
             }
     }'